<!DOCTYPE html>
<html>
<head>
<?php
if(stristr($_SERVER['HTTP_USER_AGENT'], "Mobile")){ 
?>
<link rel="stylesheet" href="styleMob.css" type="text/css" />
<?php
}
else { 
?>
<link rel="stylesheet" href="styleDesk.css" type="text/css" />
<?php
}
?>
<script>
    var state = 0;
    var noM = false;
    var repeat = false;
    
    function collapseDiv(x){   
        if(x == 1){
        document.getElementById('d02').style.visibility='hidden';
        document.getElementById('d02').style.position='absolute';
        document.getElementById('d03').style.visibility='hidden';
        document.getElementById('d03').style.position='absolute';
        state = x;
        lorem(0);
        }
        else if(x == 2){
        document.getElementById('d01').style.visibility='hidden';
        document.getElementById('d01').style.position='absolute';
        document.getElementById('d03').style.visibility='hidden';
        document.getElementById('d03').style.position='absolute';
        document.getElementById('d02').style.animationName='divAn';
        document.getElementById('d02').style.animationDuration='1s';
        <?php
                if(stristr($_SERVER['HTTP_USER_AGENT'], "Mobile")){ 
             ?>
            document.getElementById('d02').style.marginLeft='1%';
            <?php
            }
            else { 
            ?>
            document.getElementById('d02').style.marginLeft='3%';
            <?php
            }
            ?>
            
        state = x;
        lorem(1000);
        project();
        }
        else if(x == 3){
        document.getElementById('d01').style.visibility='hidden';
        document.getElementById('d01').style.position='absolute';
        document.getElementById('d02').style.visibility='hidden';
        document.getElementById('d02').style.position='absolute';
        document.getElementById('d03').style.animationName='divAn2';
        document.getElementById('d03').style.animationDuration='1s';
        <?php
                if(stristr($_SERVER['HTTP_USER_AGENT'], "Mobile")){ 
             ?>
            document.getElementById('d03').style.marginLeft='1%';
            <?php
            }
            else { 
            ?>
            document.getElementById('d03').style.marginLeft='3%';
            <?php
            }
            ?>
        state = x;
        lorem(1000);
        }
        return state;
        }
    
    function back(){
        if(state ==1){
            document.getElementById('d02').style.visibility='visible';
            document.getElementById('d02').style.position='static';
            document.getElementById('d03').style.visibility='visible';
            document.getElementById('d03').style.position='static';
            document.getElementById("A1").style.visibility='hidden';
            document.getElementById('del').innerHTML='';
            state = 0;
            noM = false;
        }
        else if(state == 2){
            document.getElementById('d02').style.animationName='divAnB1';
            document.getElementById('d02').style.animationDuration='2s';
            document.getElementById('d02').style.marginLeft='36.33%';
            document.getElementById("A1").style.visibility='hidden';
            document.getElementById('sec1').innerHTML='';
            document.getElementById('sec2').innerHTML='';
            document.getElementById('sec3').innerHTML='';
            document.getElementById('sec4').innerHTML='';
            document.getElementById('del').innerHTML='';
            
            state = 0;
            noM = false;
            setTimeout(function(){
            document.getElementById('d01').style.visibility='visible';
            document.getElementById('d01').style.position='static';
            document.getElementById('d03').style.visibility='visible';
            document.getElementById('d03').style.position='static';
                <?php
                if(stristr($_SERVER['HTTP_USER_AGENT'], "Mobile")){ 
             ?>
            document.getElementById('d02').style.marginLeft='1%';
            <?php
            }
            else { 
            ?>
            document.getElementById('d02').style.marginLeft='1%';
            <?php
            }
            ?>
            
                    
            }, 2000
                       )
        }
        else if(state == 3){
            document.getElementById('d03').style.animationName='divAnB2';
            document.getElementById('d03').style.animationDuration='2s';
            document.getElementById('d03').style.marginLeft='69.66%';
            document.getElementById("A1").style.visibility='hidden';
            document.getElementById('del').innerHTML='';
            state = 0;
            noM = false;
            
            setTimeout(function(){
            document.getElementById('d01').style.visibility='visible';
            document.getElementById('d01').style.position='static';
            document.getElementById('d02').style.visibility='visible';
            document.getElementById('d02').style.position='static';
            <?php
                if(stristr($_SERVER['HTTP_USER_AGENT'], "Mobile")){ 
             ?>
            document.getElementById('d03').style.marginLeft='1%';
            <?php
            }
            else { 
            ?>
            document.getElementById('d03').style.marginLeft='1%';
            <?php
            }
            ?>
            
            }, 2000
         )
        }
    }
    
    function changeImg(x){
        var u = 'IMG' + x;
        var o = u + "." + "2.png";
        document.getElementById(u).src = o;    
    }
    
    function changeImgB(y){
        var u = 'IMG' + y;
        var o = u + "." + "1.png";
        document.getElementById(u).src = o;
    }
    function changeImgB(z){
        var u = 'IMG' + z;
        var o = u + "." + "1.png";
        document.getElementById(u).src = o;
    }
    function changeArrow(){
        var a = "A1";
        document.getElementById(a).src ='arrow2.png';
    }
    
    function changeArrow2(){
        var a = "A1"
        document.getElementById(a).src ='arrow1.png';
    }
function lorem(x) {
    setTimeout(
      function(){
    if(noM == false){
    //prevents multiple clicks
    if(repeat == false){
    var div = document.createElement("DIV");
    div.className = "secText";
    div.id = "del";
    }
    else if(repeat == true){
    var div = document.getElementById("del")
    }
    var para = document.createElement("P");
    var title1 = document.createElement("P");
    title1.id = "secTitle";
    para.id = "lorem";
    if(repeat == false){
      }
    if(state == 1){
    var e = document.createTextNode("In a nutshell");
    var t = document.createTextNode(" I'm a 4th year software engineer student in Jyväskylä's university of applied sciences. I'm activately looking for interesting internship opportunities and a possible future job. During the years in the school I have become familiar with many programming realted skills from languages to techniques. I'm constantly expanding my knowledge in school and on my own time and always excited to learn more from my field of work. I'm familiar with many agile working frameworks and working methods and ready and capable to fill any role. I'm easy and flexible person to work with and higly motivated to succeed. I'm fluent in english and more than capable on changing language on the fly. I have experience on working with fully international teams. ");
    title1.appendChild(e);
    para.appendChild(t);
    div.appendChild(title1);
    div.appendChild(para); 
    
    var r = document.createTextNode("Skills:");
    var skill = document.createElement("P");
    skill.appendChild(r);
    skill.className = "protitR";
    skill.id = "skillTit";
    div.appendChild(skill);
    var sec = document.createElement("DIV");
    sec.className = "grade";
    var o = document.createTextNode("Programming");
    var prog = document.createElement("P");
    prog.appendChild(o);
    prog.className="prog";
    sec.appendChild(prog);
    
    var a1 = document.createTextNode("Angular");    
    var a2 = document.createTextNode("Javascript");
    var a3 = document.createTextNode("PHP");
    var a4 = document.createTextNode("C#");
    var a5 = document.createTextNode("C++");
    var a6 = document.createTextNode("SQL");
        for(i =1; i < 7; i++) {
           var di = document.createElement("DIV");
           var b = document.createElement("P");
           b.id = "b" + i;
           b.className = "gradeTex";
        if(i == 1){
            b.appendChild(a1);
            var gradImg = document.createElement("IMG");
            gradImg.className = "gradeImg"; 
            gradImg.src = "GRADE3.png";
            di.appendChild(gradImg);
        }
        else if (i == 2){
            b.appendChild(a2);
            var gradImg = document.createElement("IMG");
            gradImg.className = "gradeImg"; 
            gradImg.src = "GRADE3.png";
            di.appendChild(gradImg);
        }
        else if (i == 3){
            b.appendChild(a3);
            var gradImg = document.createElement("IMG");
            gradImg.className = "gradeImg"; 
            gradImg.src = "GRADE2.png";
            di.appendChild(gradImg);
        }
        else if (i == 4){
            b.appendChild(a4);
            var gradImg = document.createElement("IMG");
            gradImg.className = "gradeImg"; 
            gradImg.src = "GRADE3.png";
            di.appendChild(gradImg);
        }
        else if (i == 5){
            b.appendChild(a5);
            var gradImg = document.createElement("IMG");
            gradImg.className = "gradeImg"; 
            gradImg.src = "GRADE2.png";
            di.appendChild(gradImg);
        }
        else if (i == 6){
            b.appendChild(a6);
            var gradImg = document.createElement("IMG");
            gradImg.className = "gradeImg"; 
            gradImg.src = "GRADE1.png";
            di.appendChild(gradImg);
        }
           di.id = "di" + i;
           di.className = "gradeDiv";
           di.appendChild(b);
           sec.appendChild(di); 
       }
      var l = document.createTextNode("Graphical design:");
    var graph = document.createElement("P");
    graph.appendChild(l);
    graph.className="prog";
    sec.appendChild(graph);
         var a1 = document.createTextNode("Photoshop");    
    var a2 = document.createTextNode("3D-modell");
    var a3 = document.createTextNode("2D-animat");
    
        for(i =1; i < 7; i++) {
           var di = document.createElement("DIV");
           var b = document.createElement("P");
           b.id = "b" + i;
           b.className = "gradeTex";
        if(i == 1){
            b.appendChild(a1);
            var gradImg = document.createElement("IMG");
            gradImg.className = "gradeImg"; 
            gradImg.src = "GRADE4.png";
            di.appendChild(gradImg);
        }
        else if (i == 2){
            b.appendChild(a2);
            var gradImg = document.createElement("IMG");
            gradImg.className = "gradeImg"; 
            gradImg.src = "GRADE3.png";
            di.appendChild(gradImg);
        }
        else if (i == 3){
            b.appendChild(a3);
            var gradImg = document.createElement("IMG");
            gradImg.className = "gradeImg"; 
            gradImg.src = "GRADE4.png";
            di.appendChild(gradImg);
        }
           di.id = "di" + i;
           di.className = "gradeDiv";
           di.appendChild(b);
           sec.appendChild(di); 
       }
      
      div.appendChild(sec);  
      document.getElementById("sec").appendChild(div);
        
    }
    else if(state == 2){
    var e = document.createTextNode("Projects");
    var t = document.createTextNode("During the 4 years in jyväskylä's university of applied sciences I have been a part of many different big and small projects from a wide variety of subjects. ");
    title1.appendChild(e);
    para.appendChild(t);
    div.appendChild(title1);
    div.appendChild(para);  
    document.getElementById("sec").appendChild(div);
    }
    else if(state == 3){
   
    var pdf = document.createElement('EMBED');
    pdf.src ="MyPDF.pdf";
    pdf.type ="application/pdf";
    pdf.id ="pdf";
    var e = document.createTextNode("Curriculum Vitae");
    title1.appendChild(e);
    div.appendChild(title1);
    div.appendChild(para);
    div.appendChild(pdf);
    document.getElementById("sec").appendChild(div);
    
    }
    
    
    document.getElementById("A1").style.visibility='visible';
    noM = true;
    repeat = true;
    
    return noM;
    }
    }, x
    )
}
function project()
    {  
        if(noM == false){    
       for(i =1; i < 7; i++){
        var pro = document.createElement("DIV");
        
        var sect = "sec" + i;
        if(i % 2 == 0){
           pro.className = "divlink";
        }
        else{
           pro.className = "divlink2";
        }
        var pro1Img = document.createElement("IMG");
        pro1Img.className = "imglink";
        pro1Img.src = "PRO1." + i + ".png";
        var sec1 = document.createElement("SECTION");
        pro.appendChild(pro1Img);
        document.getElementById(sect).appendChild(pro);
       }
        var h1 = document.createTextNode("Boulder Pusher");
        var p1 = document.createTextNode("First major project and introduction to C# programming. Project was made for universal windows platform. The project was a great introduction to working with a team with a programming project. From a programmers point of view learned to improve my creative thinking, problem solving and C# skills. The project was made as a final projects for a course about object-oriented programming. The final product contains no ");
        var link1 = document.createElement('a');
        link1.setAttribute('href', 'https://www.youtube.com/watch?v=0IUxBi4v3jg');
        link1.innerHTML = "Link to youtube video walktrough.";
           
        var h2 = document.createTextNode("School Thesis Evaluation Tool");
        var p2 = document.createTextNode("Browser based evaluation tool for students final theses. Making a simpler way to evaluate and communicate between students trought the process of student making their final theses. At the same time the idea of the project was to deepen the knowledge of agile frameworks and the importance of documentating the project properly and troughly. Deepening introduction for html, javascript and SQL databases. The final prototype graphically and efficently improvement for the old system from users point of view. Unfortunately the example is not currently online");
        var link2 = document.createElement('a');
        link2.setAttribute('href', 'http://student.labranet.jamk.fi/~K2408/ARVIOINTI/testi2.html');
        link2.innerHTML = "Link to the tool without maintained database";    
        var h3 = document.createTextNode("Historic Tales");
        var p3 = document.createTextNode("Half a year project for a Dutch fashion museum: Modemuze. The goal of the project was to find a new innovative way to use virtual reality technology to teach children about fashion history trough action driven virtual reality storytelling. The project was developed with international team and a first touch to virtual reality technology for all of us. The project was developed with game engine unity and during the half a year in amsterdam I learned a lot about professional style of project managing and working. The project deepened my knowledge from a wide variation of fields from programming, graphical design, game design, 3D modelling, VR thecnology and storytelling. In the project we used agile framework SCRUM and I learned much about the importance of good team schechuling and planning. The final product is used occasionally in Modemuze events.");
        var link3 = document.createElement('a');
        var link4 = document.createElement('a');
        var link5 = document.createElement('a');
        link3.setAttribute('href', 'https://www.youtube.com/watch?v=T0QkwRm3Y7Q');
        link3.innerHTML = "Link to Demo 1 walktrough"; 
        link4.setAttribute('href', 'https://www.youtube.com/watch?v=ePgGM4QjMfI');
        link4.innerHTML = "Link to Demo 2 walktrough"; 
        link5.setAttribute('href', 'https://youtu.be/iH9hE9HUsAs');
        link5.innerHTML = "Link to Demo 3 walktrough";
        var h4 = document.createTextNode("Tales of Kalevala");
        var p4 = document.createTextNode("Own game project. Currently just graphical, future plans include using the sprites to make a game with unity. Work in progress.");
        var link6 = document.createElement('a');
        link6.setAttribute('href', 'https://student.labranet.jamk.fi/~K2408/HOMEPAGE/gd.pdf');
        
        link6.innerHTML = "Link to the game design document"; 
         
        var h5 = document.createTextNode("Journey for a better living");
        var p5 = document.createTextNode("Little project for the basics of PHP and web design. Journey for a better living is a personality test made for the sole purpose of mocking personality tests. I tried to make the test visually as pleasing as possible and make it as beliveable as possible. If you try it don't take anything the test says seriously.");
        var link7 = document.createElement('a');    
        link7.setAttribute('href', 'https://student.labranet.jamk.fi/~K2408/JoureyForBetterLiving/menu.php');
         link7.innerHTML = "Jorney for a better living";   
        var h6 = document.createTextNode("Landis + Gyr, Angular project");
        var p6 = document.createTextNode("As a part of course I took part in Landis + Gyr's project. For three months me and my team developed angular project in the company. The project taught me alot about the professional enviroment and working methods in the company of the field. Also it introduced and took me quite deep in Angular know-how. ");
            
        var title1 = document.createElement("P");
        var text1 = document.createElement("P");
        var title2 = document.createElement("P");
        var text2 = document.createElement("P");
        var title3 = document.createElement("P");
        var text3 = document.createElement("P");
        var title4 = document.createElement("P");
        var text4 = document.createElement("P");
        var title5 = document.createElement("P");
        var text5 = document.createElement("P");
        var title6 = document.createElement("P");
        var text6 = document.createElement("P");
        title1.className = "protitL";
        title2.className = "protitR";
        title3.className = "protitL";
        title4.className = "protitR";
        title5.className = "protitL";
        title6.className = "protitR";
        link1.className = "proLink";
        link2.className = "proLinkR";
        link3.className = "proLink";
        link4.className = "proLink";
        link5.className = "proLink";
        link6.className = "proLinkR";
        link7.className = "proLink";
        
        text1.className = "protexL";
        text2.className = "protexR";
        text3.className = "protexL";
        text4.className = "protexR";
        text5.className = "protexL";
        text6.className = "protexR";
            
        title1.appendChild(h1);
        text1.appendChild(p1);
        title2.appendChild(h2);
        text2.appendChild(p2);
        title3.appendChild(h3);
        text3.appendChild(p3);
        title4.appendChild(h4);
        text4.appendChild(p4);
        title5.appendChild(h5);
        text5.appendChild(p5);
        title6.appendChild(h6);
        text6.appendChild(p6);
        document.getElementById("sec1").appendChild(title1);
        document.getElementById("sec1").appendChild(text1);
        document.getElementById("sec1").appendChild(link1);
        document.getElementById("sec2").appendChild(title2);
        document.getElementById("sec2").appendChild(text2);
        document.getElementById("sec2").appendChild(link2);
        document.getElementById("sec3").appendChild(title3);
        document.getElementById("sec3").appendChild(text3);
        document.getElementById("sec3").appendChild(link3);
        document.getElementById("sec3").appendChild(link4);
        document.getElementById("sec3").appendChild(link5);
        document.getElementById("sec4").appendChild(title4);
        document.getElementById("sec4").appendChild(text4);
        document.getElementById("sec4").appendChild(link6);
        document.getElementById("sec5").appendChild(title5);
        document.getElementById("sec5").appendChild(text5);
        document.getElementById("sec5").appendChild(link7);
        document.getElementById("sec6").appendChild(title6);
        document.getElementById("sec6").appendChild(text6);
        }
    }
    // <a href="javascript:void(0);" onclick="javascipt:window.open('GameDesignDocument_AnttiMaaheimo.pdf');" class="popup">Clic to open.</a> 
</script>
</head>
<body>
    <header>
        <div></div>
        <div id="divport"><img id="port" src="self.png" alt="IM1">
        </div>
        <div id="divtitle">
        <p id="title"> ANTTI MAAHEIMO</p>
        </div>
        <div id="divcont">
        <p id="contact">
           +358 504681009<br>
           antmaa94@hotmail.com<br>
           Pellonreuna 2 E B 11 <br>
           40520 JYVÄSKYLÄ 
        </p>
        </div>
        
       
        
    </header>
    <section id="sec">
       
   <img class="A" id="A1" src="arrow1.png" alt="arrow"
                                          onmouseover= "changeArrow()" 
                                          onmouseout= "changeArrow2()"
                                          onclick="back()">
    <div id="d01" class="divlink">   <img class="imglink" id="IMG1"  src="IMG1.1.png" alt="IM1" 
                                          onmouseover= "changeImg(1);"
                                          onmouseout= "changeImgB(1);"
                                          onclick= "collapseDiv(1);"></div>
    <div id="d02" class="divlink">   <img class="imglink" id="IMG2" src="IMG2.1.png" alt="IM2"
                                          onmouseover= "changeImg(2);"
                                          onmouseout= "changeImgB(2);"
                                          onclick= collapseDiv(2);></div>
    <div id="d03" class="divlink">   <img class="imglink" id="IMG3" src="IMG3.1.png" alt="IM3" 
                                          onmouseover= "changeImg(3);"
                                          onmouseout= "changeImgB(3);"
                                          onclick= collapseDiv(3);></div>
        
    </section>
    <section id="sec1">
    </section>
    <section id="sec2">
    </section>
    <section id="sec3">
    </section>
    <section id="sec4">
    </section>
    <section id="sec5">
    </section>
    <section id="sec6">
    </section>
<footer>
   All rights reserved
</footer>
</body>
</html>